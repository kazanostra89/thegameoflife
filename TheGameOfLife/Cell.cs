﻿using System;

namespace TheGameOfLife
{
    enum Gender
    {
        Man = 0,
        Woman = 1
    }

    class Cell
    {
        private Gender cellGender;
        private ConsoleColor colorGender;
        private int x, y;
        private int health;
        private char cellChar = '*';
        //private char cellChar;

        public Cell()
        {
            cellGender = Gender.Man;
            colorGender = ConsoleColor.Red;
            x = y = 1;
            health = 10;
        }

        public Cell(int x, int y, Gender cellGender, int health)
        {
            this.x = x;
            this.y = y;
            this.cellGender = cellGender;
            this.health = health;

            if (cellGender == Gender.Man)
            {
                colorGender = ConsoleColor.Red;
                //cellChar = '♂';
            }
            else
            {
                colorGender = ConsoleColor.Yellow;
                //cellChar = '♀';
            }
        }

        public void StepCell(int x, int y)
        {
            this.x += x;
            this.y += y;
        }

        public void FirstUniqueCoord(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void PrintCell()
        {
            Console.ForegroundColor = colorGender;
            Console.SetCursorPosition(x, y);
            Console.Write(cellChar);
        }

        public int X
        {
            set { x = value; }

            get { return x; }
        }

        public int Y
        {
            set { y = value; }

            get { return y; }
        }

        public int Health
        {
            set { health = value; }

            get { return health; }
        }

        public Gender CellGender
        {
            get { return cellGender; }
        }
    }
}
