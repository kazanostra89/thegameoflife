﻿using System;

namespace TheGameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            CellManager manager = new CellManager(3, Console.WindowWidth - 3, 0, Console.WindowHeight - 1, 50);
            
            manager.StartGame();

            Console.ReadKey();
        }
    }
}
