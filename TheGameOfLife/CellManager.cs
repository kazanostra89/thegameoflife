﻿using System;

namespace TheGameOfLife
{
    class CellManager
    {
        private Random rnd;
        private Cell[] cells;
        private int minX, maxX, minY, maxY;

        public CellManager(int minX, int maxX, int minY, int maxY, int countCells)
        {
            this.minX = minX;
            this.maxX = maxX;
            this.minY = minY;
            this.maxY = maxY;

            rnd = new Random();
            cells = new Cell[countCells];

            FirstGenerationCells();
        }

        private void FirstGenerationCells()
        {
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i] = new Cell(rnd.Next(minX, maxX + 1), rnd.Next(minY, maxY + 1), Gender.Man, rnd.Next(5, 10 + 1));
            }

            SearchMatchFirstGenerationCoord();
        }

        private void SearchMatchFirstGenerationCoord()
        {
            for (int i = 0; i < cells.Length; i++)
            {
                for (int j = i + 1; j < cells.Length; j++)
                {
                    if (cells[i].X == cells[j].X && cells[i].Y == cells[j].Y)
                    {
                        int x, y;
                        GenerationUniqueCoord(out x, out y);
                        cells[j].FirstUniqueCoord(x, y);
                    }
                }
            }
        }

        private void GenerationUniqueCoord(out int x, out int y)
        {
            bool randomG;

            do
            {
                randomG = false;

                x = rnd.Next(minX, maxX + 1);
                y = rnd.Next(minY, maxY + 1);

                for (int i = 0; i < cells.Length; i++)
                {
                    if (cells[i].X == x && cells[i].Y == y)
                    {
                        randomG = true;
                        break;
                    }
                }

            } while (randomG);
        }

        public void PrintArrayCells()
        {
            for (int i = 0; i < cells.Length; i++)
            {
                cells[i].PrintCell();
            }
        }

        public void StepsCells()
        {
            int x, y;

            for (int i = 0; i < cells.Length; i++)
            {
                do
                {
                    x = rnd.Next(-1, 2);
                    y = rnd.Next(-1, 2);

                } while ((x == 0 && y == 0) || cells[i].Y + y < 0 || cells[i].X + x < 0
                          || cells[i].X + x >= Console.WindowWidth
                          || cells[i].Y + y >= Console.WindowHeight);

                cells[i].StepCell(x, y);
            }
        }

        private void AddNewCell()
        {
            int x, y;

            Cell[] newCells = new Cell[cells.Length + 1];

            for (int i = 0; i < cells.Length; i++)
            {
                newCells[i] = cells[i];
            }

            GenerationUniqueCoord(out x, out y);

            newCells[newCells.Length - 1] = new Cell(x, y, Gender.Man, rnd.Next(5, 10 + 1));

            cells = newCells;
        }

        private void DeathCell(int position)
        {
            Cell[] newCells = new Cell[cells.Length - 1];

            int newIndex = 0;

            for (int i = 0; i < cells.Length; i++)
            {
                if (i != position)
                {
                    newCells[newIndex] = cells[i];
                    newIndex++;
                }
            }

            cells = newCells;
        }

        private void SearchZeroHealth()
        {
            for (int i = 0; i < cells.Length; i++)
            {
                if (cells[i].Health <= 0)
                {
                    DeathCell(i);
                }
            }
        }

        private void SearchMatchManWomanCells()
        {
            SearchZeroHealth();

            for (int i = 0; i < cells.Length; i++)
            {
                for (int j = i + 1; j < cells.Length; j++)
                {
                    if (cells[i].X == cells[j].X && cells[i].Y == cells[j].Y && cells[i].CellGender != cells[j].CellGender)
                    {
                        AddNewCell();
                    }
                }
            }
        }

        private void SearchEqualityGenderCells()
        {
            for (int i = 0; i < cells.Length; i++)
            {
                for (int j = i + 1; j < cells.Length; j++)
                {
                    if (cells[i].X == cells[j].X && cells[i].Y == cells[j].Y && cells[i].CellGender == cells[j].CellGender)
                    {
                        cells[i].Health = cells[i].Health / 2;
                        cells[j].Health = cells[j].Health / 2;
                    }
                }
            }

            SearchZeroHealth();
        }

        public void StartGame()
        {
            int numberIterations = 0;

            while (cells.Length != 10)
            {
                numberIterations++;

                Console.Clear();
                StepsCells();
                SearchMatchManWomanCells();
                SearchEqualityGenderCells();
                PrintArrayCells();
            }

            Console.ReadKey();
        }



        public void SearchMatchCells()
        {
            for (int i = 0; i < cells.Length; i++)
            {
                for (int j = i + 1; j < cells.Length; j++)
                {
                    if (cells[i].X == cells[j].X && cells[i].Y == cells[j].Y)
                    {
                        if (cells[i].CellGender != cells[j].CellGender)
                        {
                            AddNewCell();
                        }
                        else
                        {
                            if (cells[i].Health > cells[j].Health)
                            {
                                //cells[i].Health += cells[j].Health / 2;
                                DeathCell(j);
                            }
                            else if (cells[i].Health < cells[j].Health)
                            {
                                //cells[j].Health += cells[i].Health / 2;
                                DeathCell(i);
                            }
                            else
                            {
                                cells[i].Health = cells[i].Health / 2;
                                cells[j].Health = cells[j].Health / 2;

                            }
                        }
                    }
                }
            }
        }

    }
}
